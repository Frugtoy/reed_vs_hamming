//
// Created by root on 18.05.2020.
//

#define STB_IMAGE_IMPLEMENTATION
#define STB_IMAGE_WRITE_IMPLEMENTATION

#include "ReedVsHamming.h"
#include "../hammingCode/hammingCode.h"
#include <cstring>
#include<string>
using std::string;
ReedVsHamming::ReedVsHamming(char * path , char* name_of_downloaded){
    
    this->image = stbi_load(path_to_img(path,name_of_downloaded),&this->pic_width,&this->pic_height,&this->pic_channels,STBI_rgb);
    if (!this->image){
        const char * err = stbi_failure_reason();
        if (err)
        {
            std::cout<<std::endl<<err;
            throw 1;
        }
    }
    /*corrupting an image copy and load pic into images.*/
    //Reed logic
	try {
		this->hamming(path);
	}
	catch (...) {
		throw 2;
	}
    std::cout<<"ok"<<std::endl;
    try {
		this->reed(path);
	}
	catch (...) {
		throw 3;
	}
    

    
    //Hamming logic
    
}
ReedVsHamming::~ReedVsHamming(){
    stbi_image_free(image);
}

void ReedVsHamming::reed(char*path){
    std::cout << "----------------reed-------------------------\n";
     
    int size = this->pic_width * this->pic_height * this->pic_channels;
    vector<int>blink = corrupt(size);//getting the corruption rule
    uint8_t* buf = new uint8_t[size];
    vector<vector<uint8_t>> data;
    vector<ezpwd::RS<255,251>> reed;
    //ezpwd::RS<255,251> rs;
    int idx = -1;
    int size_of_last =252;

    //собираем вектор и rs для каждого
    for( int i = 0; i < size; ++i ){

        if((i != 0)&&(size_of_last < 251)){
            data[idx].push_back(this->image[i]);
            size_of_last++;
        }

        else{
            data.push_back(vector<uint8_t>());
            reed.push_back(ezpwd::RS<255,251>());
            idx += 1;
            data[idx].push_back(this->image[i]);
            size_of_last = 1;
        }
    }
    std::cout << std::endl << size_of_last << " " << data.size() << std::endl;
    for(int i = size_of_last; i < 251; ++ i){
        data[idx].push_back('0');
    }


    for( int i =0; i< idx + 1 ; ++i ){
        reed[i].encode( data[i] );

    }

    

//loading corrapted photo into buffer
    int symb = 0;


    for( int i = 0; i < idx; ++i ){
        for( int j = 0 ; j < 251; ++j ){
            buf[symb] = data[i][j];
            ++symb;
        }
    }
    for( int i = 0; i< 251, symb < size;++i){
        buf[symb] = data[idx][i];
        symb++;
    }
// getting rule 
    for ( int i =0 ; i<blink.size();++i ) {
        buf[ blink[i] ] = 0;
    }
    //data/chat_id + name
    //corruptedReed.jpg
    const char* path_to_corrupted_reed = path_to_img(path,"corruptedReed.jpg");
    stbi_write_jpg(path_to_corrupted_reed,this->pic_width,this->pic_height,this->pic_channels,buf,100);
    delete[] path_to_corrupted_reed;

//decoding/**/
    for ( int i = 0; i< idx + 1;++i ){ //decoding
        int fixed = reed[i].decode( data[i] );
        data[i].resize( data[i].size() - reed[i].nroots() );
    }
    symb =0;
    for( int i = 0; i < idx; ++i ){
        for( int j = 0 ; j < data[i].size(); ++j ){
            buf[symb] = data[i][j];
            ++symb;
        }
    }
    for( int i =0; i< data[idx].size(),symb < size;++i){
        buf[symb] = data[idx][i];
        symb++;
    }/**/
    const char* path_to_fixed_reed= path_to_img(path,"fixedReed.jpg");
    stbi_write_jpg(path_to_fixed_reed, this->pic_width, this->pic_height, this->pic_channels, buf, 100 );
    delete[] path_to_fixed_reed;
    delete buf;


}

void ReedVsHamming::drawPic(std::vector<uint8_t> arrImage, const char* name)
{
    int size = this->pic_width * this->pic_height * this->pic_channels;
    uint8_t* buf = new uint8_t[size];

    for(int i=0; i < size; ++i)
    {
        buf[i] = arrImage[i];
    }

    stbi_write_jpg(name, this->pic_width, this->pic_height, this->pic_channels, buf,100);

    delete buf;
}


std::vector<int> ReedVsHamming::corrupt(int size)
{
    


    std::random_device rd;
    std::mt19937 generator(rd());
    std::uniform_int_distribution<int> distribution(0,size-1);

    int vec_size = distribution(generator);
    vector<int> rule(0);

    for(int i = 0; i < vec_size; ++i){
        rule.push_back(distribution(generator));
    }
    return rule;
}


void ReedVsHamming::hamming(char*path)
{
    std::cout << "----------------hamming-------------------------\n";
    hammingCode HC;
    std::vector<uint8_t> vImage(0);
    int size = this->pic_width * this->pic_height * this->pic_channels;
    int index = 0;

    while (index < size)
    {
        vImage.push_back(this->image[index]);
        ++index;
    }

    HC.encode(vImage, HC.encodedBytes, HC.arrOfThreeBytes);

    std::vector<std::vector<uint8_t>> copy(HC.encodedBytes);

    HC.corrupt(HC.encodedBytes, this->corrupt(size));

    HC.compare(HC.encodedBytes, copy);

    HC.decode(HC.encodedBytes, HC. decodedImage, vImage.size());
    HC.fix(HC.encodedBytes, HC.arrOfThreeBytes, HC.firstByteError, HC.secondByteError);
    const char* path_to_corrupted_hamming = path_to_img(path,"corruptedHamming.jpg");
    this->drawPic(HC.decodedImage, path_to_corrupted_hamming);
    delete[] path_to_corrupted_hamming;
    HC. decode(HC.encodedBytes, HC.decodedImage, vImage.size());
    const char* path_to_fixed_hamming = path_to_img(path,"fixedHamming.jpg");
    this->drawPic(HC.decodedImage,path_to_fixed_hamming);
    delete[] path_to_fixed_hamming;
}
const char* ReedVsHamming:: path_to_img( char * a, char *b){

    string first = a;
    string second = b;
    first+=second;
    char*dest = new char[first.size()+1];
    std::copy(first.begin(),first.end(),dest);
    dest[first.size()]='\0';
    std::cout<<*dest<<std::endl;
    return dest;
}
