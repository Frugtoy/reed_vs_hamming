//
// Created by root on 18.05.2020.


#ifndef HAMMINGCODE_REEDVSHAMMING_H
#define HAMMINGCODE_REEDVSHAMMING_H

#include "../third_party/stb_image.h"
#include "../third_party/stb_image_write.h"
#include "../third_party/c++/ezpwd/rs"

//#include "stb_image.h"
//#include "stb_image_write.h"
//#include "rs"

#include <cstddef>
#include <iostream>
#include <string>

using std::vector;

class ReedVsHamming{

public:

    ReedVsHamming(char* path,char * name_of_downloaded);
    ~ReedVsHamming();
    void loadPic(const char*path); /// что ита??
    void drawPic(std::vector<uint8_t> arrImage, const char* name);
    std::vector<int> corrupt(int size);
    const char* path_to_img( char * a, char *b);
    //void corrupt();//coming soon
    /// Solomon-Reed
    void reed(char*path);
    /// Hamming
    void hamming(char*path);

private:

    stbi_uc* image;
    int pic_width,pic_height,pic_channels;


};

#endif //HAMMINGCODE_REEDVSHAMMING_H
