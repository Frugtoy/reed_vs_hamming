#define CATCH_CONFIG_MAIN
#define STB_IMAGE_IMPLEMENTATION
#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "../ReedVsHamming/ReedVsHamming.h"
#include "../third_party/catch.hpp"
#include "../hammingCode/hammingCode.h"


bool pic_is_ex(const char* path){
    int x(0),y(0),channels_in_file(0),desired_channels(0);
    stbi_load(path,&x,&y,&channels_in_file,desired_channels);
    //std::cout<<x<<" "<<y<<" "<<channels_in_file<<" "<<desired_channels<<std::endl<<std::endl;
    if(x+y+channels_in_file+desired_channels == 0){return false;}

return true;
}

TEST_CASE("JUST 4 FUN"){
    SECTION("FUN") {
        REQUIRE(
            2 == 2
        );
    }

    SECTION("pic loaded"){ 
     REQUIRE(pic_is_ex("non_existing_one.jpg")== false);
    };
}

bool compare (std::vector<uint8_t> before, std::vector<uint8_t> after, char flag, int size = 0)
{
    int counter{0};

    for (int i=0; i<before.size(); ++i)
    {
        if (before[i] != after[i]) {
            ++counter;
        }
    }

    if (flag == 'c')
    {
        if (counter >= (size/3)*2)
        {
            return true;
        }
    }

    if (flag == 'f')
    {
        if (counter == 0)
        {
            return true;
        }
    }

    return false;
}

hammingCode HaCo;

std::vector<uint8_t> before{9,8,7,6,5,4,3,2,1,0};
std::vector<int> rule{1,3,5,7,9};

TEST_CASE ("проверка работы функции corrupt")
{
    SECTION("hamming")
    {
        std::vector<uint8_t> after(0);
        ThreeBytesArr arr;

        std::vector<std::vector<uint8_t>> encodedVector(0);

        HaCo.encode(before, encodedVector, arr);
        HaCo.corrupt(encodedVector, rule);
        HaCo.decode(encodedVector, after, before.size());

        REQUIRE( compare(before, after, 'c', rule.size()) == true );
    }
}


TEST_CASE("проверка работы функции fix")
{
    SECTION("hamming")
    {
        std::vector<uint8_t> after(0);
        ThreeBytesArr arr;
        int firstError;
        int secondError;
        std::vector<std::vector<uint8_t>> encodedVector(0);

        HaCo.encode(before, encodedVector, arr);
        HaCo.corrupt(encodedVector, rule);
        HaCo.fix(encodedVector, arr, firstError, secondError);
        HaCo.decode(encodedVector, after, before.size());

        REQUIRE( compare(before, after, 'f') == true );
    }
}

