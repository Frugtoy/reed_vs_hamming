//
// Created by root on 07.05.2020.
//

#include <iostream>
#include "hammingCode.h"

//hammingCode HC;


void hammingCode::writeByte (uint8_t _byte)
{
    for (int i = 0; i < 8; ++i)
    {
        std::cout << bitRead(_byte, i) ? '1' : '0';
    }
    std::cout << "\n";
}

void hammingCode::addAndShuffle (ThreeBytesArr &arrOfThreeBytes, uint8_t _byte1, uint8_t _byte2)
{
    for (int i=0; i<3; ++i)
    {
        arrOfThreeBytes[i] = 0;
    }

    uint8_t currentIByte = _byte1;
    uint8_t currentIBit = 0;

    for (int i=0; i<24; ++i)
    {
        int OutBitNum = i % 8;
        int OutByteNum = i / 8;
        //0,1,3,7,12,13,15,19
        switch (i)
        {
            case 0:
            case 1:
            case 3:
            case 7:
            case 12:
            case 13:
            case 15:
            case 19:
            {
                continue;
                //break;
            }
            default: {
                bitWrite(arrOfThreeBytes[OutByteNum], OutBitNum, bitRead(currentIByte, currentIBit));
                ++currentIBit;
            }

        }

        if (currentIBit == 8)
        {
            currentIBit = 0;
            currentIByte = _byte2;
        }
    }
}

void hammingCode::countBits(ThreeBytesArr &arrOfThreeBytes)
{
    int currentByte{0};
    int currentBit{0};


    for (uint8_t i=0; i<2; ++i) {

        int firstBit{0};
        int secondBit{0};
        int forthBit{0};
        int eightsBit{0};

        for (uint8_t j = 0; j < 12; ++j) {
            switch (j) {
                case 2:
                    firstBit += bitRead(arrOfThreeBytes[currentByte], currentBit);
                    secondBit += bitRead(arrOfThreeBytes[currentByte], currentBit);
                    break;
                case 4:
                    firstBit += bitRead(arrOfThreeBytes[currentByte], currentBit);
                    forthBit += bitRead(arrOfThreeBytes[currentByte], currentBit);
                    break;
                case 5:
                    secondBit += bitRead(arrOfThreeBytes[currentByte], currentBit);
                    forthBit += bitRead(arrOfThreeBytes[currentByte], currentBit);
                    break;
                case 6:
                    firstBit += bitRead(arrOfThreeBytes[currentByte], currentBit);
                    secondBit += bitRead(arrOfThreeBytes[currentByte], currentBit);
                    forthBit += bitRead(arrOfThreeBytes[currentByte], currentBit);
                    break;
                case 8:
                    firstBit += bitRead(arrOfThreeBytes[currentByte], currentBit);
                    eightsBit += bitRead(arrOfThreeBytes[currentByte], currentBit);
                    break;
                case 9:
                    secondBit += bitRead(arrOfThreeBytes[currentByte], currentBit);
                    eightsBit += bitRead(arrOfThreeBytes[currentByte], currentBit);
                    break;
                case 10:
                    firstBit += bitRead(arrOfThreeBytes[currentByte], currentBit);
                    secondBit += bitRead(arrOfThreeBytes[currentByte], currentBit);
                    eightsBit += bitRead(arrOfThreeBytes[currentByte], currentBit);
                    break;
                case 11:
                    forthBit += bitRead(arrOfThreeBytes[currentByte], currentBit);
                    eightsBit += bitRead(arrOfThreeBytes[currentByte], currentBit);
                    break;
                default:;
            }
            ++currentBit;
            if (currentBit == 8) {
                ++currentByte;
                currentBit = 0;
            }
        }

        if (i == 0) {
            bitWrite(arrOfThreeBytes[0], 0, firstBit % 2);
            bitWrite(arrOfThreeBytes[0], 1, secondBit % 2);
            bitWrite(arrOfThreeBytes[0], 3, forthBit % 2);
            bitWrite(arrOfThreeBytes[0], 7, eightsBit % 2);

        } else
        {
            bitWrite(arrOfThreeBytes[1], 4, firstBit % 2);
            bitWrite(arrOfThreeBytes[1], 5, secondBit % 2);
            bitWrite(arrOfThreeBytes[1], 7, forthBit % 2);
            bitWrite(arrOfThreeBytes[2], 3, eightsBit % 2);
        }
    }
}

void hammingCode::encode (std::vector<uint8_t> image, std::vector<std::vector<uint8_t>> &encodedBytes, ThreeBytesArr &_arrOfThreeBytes)
{
    if (image.size()%2 == 0)
    {
        for (int arrInd = 0; arrInd < image.size(); arrInd += 2 )
        {
            addAndShuffle(_arrOfThreeBytes, image[arrInd], image[arrInd + 1]);
            countBits(_arrOfThreeBytes);
            encodedBytes.push_back({0, 0, 0});
            for (int i=0; i<3; ++i)
            {
                encodedBytes[encodedBytes.size()-1][i] = _arrOfThreeBytes[i];
             }
         }
    } else // if (image.size()%2 == 1)
    {
        for(int arrInd=0; arrInd < (image.size()-1); arrInd+=2 )
        {
            addAndShuffle(_arrOfThreeBytes, image[arrInd], image[arrInd + 1]);
            countBits(_arrOfThreeBytes);

            encodedBytes.push_back({0, 0, 0});
            for (int i=0; i<3; ++i)
            {
                encodedBytes[encodedBytes.size()-1][i] = _arrOfThreeBytes[i];
            }
        }

        encodedBytes.push_back({0, 0, 0});
        addAndShuffle(_arrOfThreeBytes, image[image.size() - 1]);
        countBits(_arrOfThreeBytes);
        for (int i=0; i<3; ++i)
        {
            encodedBytes[(encodedBytes.size()-1)][i] = _arrOfThreeBytes[i];
        }
    }

}

void hammingCode::deleteBits (std::vector<uint8_t> &encodedBytes, std::vector<uint8_t> &decodedBytes) {
    int currentByte = 0; //// отслеживает текущий байт в decodedBytes
    int currentBit = 0; //// отслеживает позицию бита данных в массиве decodedBytes

    int index = 0;
    while( index < encodedBytes.size() )
    {
        for (int i =0; i<24; ++i)
        {
            if ( (i%8 == 0) && (i!=0) )
            {
                ++index;
            }
            switch (i) {
                case 0:
                case 1:
                case 3:
                case 7:
                case 12:
                case 13:
                case 15:
                case 19:
                    //case 24:
                    break;

                default:
                    bitWrite(decodedBytes[currentByte], currentBit, bitRead(encodedBytes[index], i % 8));
                    currentBit++;
            }

            if (currentBit == 8)
            {
                currentBit = 0;
                ++currentByte;
            }
        }
        ++index;
        currentBit = 0;
    }
}

void hammingCode::decode (std::vector<std::vector<uint8_t>> &encodedBytes, std::vector<uint8_t> &decodedImage, int size)
{
    decodedImage.clear();

    std::vector<uint8_t> decodedBytes{0};

    for (int k=0; k < encodedBytes.size(); ++k)
    {
        decodedBytes.clear();
        for (int i = 0; i < ((encodedBytes[k].size()) / 3) * 2; ++i)
        {
            decodedBytes.push_back(0);
        }

        deleteBits(encodedBytes[k], decodedBytes);

        for(int a=0; a<decodedBytes.size(); ++a)
        {
            decodedImage.push_back(decodedBytes[a]);
        }

        while (size < decodedImage.size())
        {
            decodedImage.pop_back();
        }

    }
}


void hammingCode::compare(std::vector<uint8_t> &corruptedBytes, ThreeBytesArr &arrOfThreeBytes, int &firstByteError, int &secondByteError)
{
    firstByteError = 0;
    secondByteError = 0;

    //0,1,3,7,12,13,15,19
    for (int currentBit=0; currentBit < 24; ++currentBit)
    {

        switch(currentBit)
        {
            case 0:
            case 1:
            case 3:
            case 7:
            case 12:
            case 13:
            case 15:
            case 19:
                if (bitRead(corruptedBytes[currentBit / 8], currentBit % 8) != bitRead(arrOfThreeBytes[currentBit / 8], currentBit % 8))
                {
                    if (currentBit < 8)
                    {
                        firstByteError += currentBit;
                        ++firstByteError;
                    }else{
                        secondByteError += (currentBit - 12);
                        ++secondByteError;
                    }
                }

            default:
                ;
        }
    }

    if (firstByteError > 0)
    {
        firstByteError -= 1;
    } else{
        firstByteError -= 25;
    }

    if (secondByteError > 0)
    {
        secondByteError += 11;
    } else{
        secondByteError -= 25;
    }
}

void hammingCode::changeBits (std::vector<uint8_t> &arr, int &firstByteError, int &secondByteError)
{
    if (firstByteError > 0)
    {
        if ( bitRead(arr[firstByteError/8], firstByteError%8) == 0)
        {
            bitWrite(arr[firstByteError/8], firstByteError%8, 1);
        } else
        {
            bitWrite(arr[firstByteError/8], firstByteError%8, 0);
        }
    }
    if (secondByteError > 0)
    {
        if ( bitRead(arr[secondByteError/8], secondByteError%8) == 0)
        {
            bitWrite(arr[secondByteError/8], secondByteError%8, 1);
        } else
        {
            bitWrite(arr[secondByteError/8], secondByteError%8, 0);
        }
    }
}

void hammingCode::fix (std::vector<std::vector<uint8_t>> &corruptedBytes, ThreeBytesArr &arrOfThreeBytes, int &firstByteError, int &secondByteError)
{
    for (int i=0; i < corruptedBytes.size(); ++i)
    {
        for (int j=0; j < 3; ++j)
        {
            arrOfThreeBytes[j] = corruptedBytes[i][j];
        }
        countBits(arrOfThreeBytes);
        compare(corruptedBytes[i], arrOfThreeBytes, firstByteError, secondByteError);
        changeBits(corruptedBytes[i], firstByteError, secondByteError);
    }
}

void hammingCode::corrupt (std::vector<std::vector<uint8_t>> &encodedBytes, std::vector<int> bytePositions)
{
    for (int i=0; i<bytePositions.size(); ++i)
    {
        int randomBit{0};
        int unusedBit{-30};

        std::random_device rd;
        std::mt19937 gen(rd());

        randomBit = abs(gen());

        if (bytePositions[i]%2 == 0)
        {
            /// расчет случайного значения в рамках [0; 11]
            randomBit%=12;
            changeBits(encodedBytes[bytePositions[i]/2], randomBit, unusedBit);
        } else{
            /// расчет случайного значения в рамках [12; 23]
            randomBit %= 12;
            randomBit += 12;
            changeBits(encodedBytes[bytePositions[i]/2], unusedBit, randomBit);
        }
    }
}

void hammingCode::compare(std::vector<std::vector<uint8_t>> &corruptedBytes, std::vector<std::vector<uint8_t>> &copy)
{
    int counter = 0;
    for (int i=0; i < corruptedBytes.size(); ++i)
    {
        for (int j=0; j<corruptedBytes[i]. size(); ++j)
        {
            if (corruptedBytes[i][j] != copy[i][j])
            {
                //std::cout << "Произошло изменение в байте [" << i << "] [" << j << "]\n";
                ++counter;
            }
        }
    }
    if (counter == 0)
    {
        std::cout << "Изменений не произошло\n";
    }
}