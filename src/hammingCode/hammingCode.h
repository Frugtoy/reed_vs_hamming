//
// Created by root on 07.05.2020.
//
#pragma once
#ifndef HAMMINGCODE_HAMMINGCODE_H
#define HAMMINGCODE_HAMMINGCODE_H

#include <cstdint>
#include <vector>
#include <random>

#define bitRead(value, bit) (((value) >> (bit)) & 0x01) //// выдает значение заданного бита
#define bitSet(value, bit) ((value) |= (1UL << (bit))) //// на заданную позицию записывает 1
#define bitClear(value, bit) ((value) &= ~(1UL << (bit))) //// на заданную позицию записывает 0
#define bitWrite(value, bit, bitvalue) (bitvalue ? bitSet(value, bit) : bitClear(value, bit)) /*Записыввает на заданную позицию заданное значение*/

struct byte
{
    uint8_t p1: 1;
    uint8_t p2: 1;
    uint8_t p3: 1;
    uint8_t p4: 1;
    uint8_t p5: 1;
    uint8_t p6: 1;
    uint8_t p7: 1;
    uint8_t p8: 1;
    byte()
    {
        p1 = 0;
        p2 = 0;
        p3 = 0;
        p4 = 0;
        p5 = 0;
        p6 = 0;
        p7 = 0;
        p8 = 0;
    }
};

typedef uint8_t ThreeBytesArr[3];


class hammingCode
{
public:

    hammingCode () = default;
    ~hammingCode () = default;

    void writeByte (uint8_t _byte);

    void addAndShuffle (ThreeBytesArr &arr, uint8_t _byte1, uint8_t _byte2=0);
    void countBits(ThreeBytesArr &arrOfThreeBytes);

    void deleteBits (std::vector<uint8_t> &encodedBytes, std::vector<uint8_t> &decodedBytes);

    /// посчитает позицию ошибки
    void compare(std::vector<uint8_t> &corruptedBytes, ThreeBytesArr &arrOfThreeBytes, int &firstByteError, int &secondByteError);
    /// изменит "поломанный бит данных"
    void changeBits (std::vector<uint8_t> &arr, int &firstByteError, int &secondByteError);

    void compare(std::vector<std::vector<uint8_t>> &corruptedBytes, std::vector<std::vector<uint8_t>> &copy);

    //// кодирует
    void encode (std::vector<uint8_t> image, std::vector<std::vector<uint8_t>> &encodedBytes, ThreeBytesArr &_arrOfThreeBytes);
    //// декодирует
    void decode (std::vector<std::vector<uint8_t>> &encodedBytes, std::vector<uint8_t> &decodedImage, int size);
    //// исправляет ошибки
    ///*std::vector<uint8_t>*/ void fix (std::vector<uint8_t> &corruptedBytes, ThreeBytesArr &arrOfThreeBytes, ThreeBytesArr &countedArrOfThreeBytes);
    void fix (std::vector<std::vector<uint8_t>> &corruptedBytes, ThreeBytesArr &arrOfThreeBytes, int &firstByteError, int &secondByteError);


    //// корраптит массив с закодированным изображением
    void corrupt (std::vector<std::vector<uint8_t>> &encodedBytes, std::vector<int> bytePositions);

    int firstByteError;
    int secondByteError;

    ThreeBytesArr arrOfThreeBytes;
    std::vector<uint8_t> decodedImage;
    std::vector<std::vector<uint8_t>> encodedBytes;

};

//extern hammingCode HC;

#endif //HAMMINGCODE_HAMMINGCODE_H

