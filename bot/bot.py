import telebot
import time
import subprocess
import os
token=os.environ['BOT_TOKEN']

bot = telebot.TeleBot(token)



@bot.message_handler(commands=['start'])
def start_message(message):
    bot.send_message(message.chat.id, 'Рад приветствовать тебя, пришли мне фото в jpg формате, я испорчу его в рандомных местах, а алгоритм Хэмминга и Соломона-Рида пофиксят его, или нет XD')


@bot.message_handler(content_types=['photo'])
def handle_docs_photo(message):
    if os.path.isdir('/data/'+str(message.chat.id)):
        bot.reply_to( message,"не спеши, я воюю с прошлым фото, отправь ещё разок, после того как я закончу" )
    else:    
        try:
            mkdir = "mkdir "+str(message.chat.id)
            rm_dir = "rm -rf "+ "/data/"+ str(message.chat.id)#soon
       # print(mkdir)
            os.system(mkdir)
            src='./'+str(message.chat.id) + '/'
            runcmd = './reed_vs_hamming-linux-x86 '
            flag = 0
            file_info = bot.get_file(message.photo[len(message.photo)-1].file_id)
            downloaded_file = bot.download_file(file_info.file_path)
           
            with open(src + "downloaded.jpg", 'wb') as new_file:
                    
                new_file.write(downloaded_file)
                bot.send_message(message.chat.id, 'Обработка может занять некоторое время (20 секунд)')
                bot.send_chat_action (message.chat.id,"upload_photo")
            
                run = runcmd + src + " downloaded.jpg"
           
            proc = subprocess.Popen(run , shell = True)
            err_code = proc.wait()
            print(err_code)
            if err_code:
                raise ValueError(err_code)
            
            with open(src +"corruptedHamming.jpg",'rb') as corrupted:
                bot.send_chat_action (message.chat.id,"upload_photo")
                bot.send_photo(message.from_user.id,corrupted,caption='Вот как фото испортилось в Хэмминге пока доходило до меня...')

            with open(src + "corruptedReed.jpg",'rb') as corrupted:
                bot.send_chat_action (message.chat.id,"upload_photo")
                bot.send_photo(message.from_user.id,corrupted,caption='Вот как фото испортилось в Соломоне-Риде пока доходило до меня...')
        
            with open(src + "fixedReed.jpg",'rb') as reed:
                bot.send_chat_action (message.chat.id,"upload_photo")
                bot.send_photo(message.from_user.id,reed,caption='Вот как фото пофиксил код Соломона-Рида...')
            with open(src + "fixedHamming.jpg",'rb') as reed:
                bot.send_chat_action (message.chat.id,"upload_photo")
                bot.send_photo(message.from_user.id,reed,caption='Вот как фото пофиксил код Хэмминга...')
                os.system(rm_dir)
        
        except ValueError as e:
            bot.reply_to(message, str(e))
            bot.reply_to( message,e )
            os.system(rm_dir)
            print(e)
        
        except Exception as e:
            bot.reply_to( message,"Фото очень сильно испортилось, боюсь мы его потеряли попробуй кинуть еще разок : - (" )
            bot.reply_to( message,e )
            os.system(rm_dir)
            print(e)

@bot.message_handler(content_types =['text', 'audio', 'document', 'sticker', 'video', 'video_note', 'voice', 'location', 'contact', 'new_chat_members', 'left_chat_member', 'new_chat_title', 'new_chat_photo', 'delete_chat_photo', 'group_chat_created', 'supergroup_chat_created', 'channel_chat_created', 'migrate_to_chat_id', 'migrate_from_chat_id', 'pinned_message'])
def alarm(message):
    bot.reply_to( message,"Пользователь должен был отправить картинку в jpg формате но не сделал этого. Пришли мне картинку)))" )

        
bot.polling()
