#! /usr/bin/env bash

python3 -m pip install -r requirements.txt;
mkdir img
mkdir dir_bot
mv bot* dir_bot
kill $(cat ./bot_pid) || true
nohup python3 ./dir_bot/bot.py >log.txt 2>&1 & 
echo $! > ./bot_pid
