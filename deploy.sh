#! /usr/bin/env bash

export SSHPASS=$SSH_PASS

sshpass -e scp -o StrictHostKeyChecking=no -P $SSH_PORT \
    ./build/reed_vs_hamming-linux-x86 $SSH_USER@$SSH_HOST:/data/

sshpass -e scp -o StrictHostKeyChecking=no -P $SSH_PORT \
    -r ./bot/* $SSH_USER@$SSH_HOST:/data/

sshpass -e ssh -n -oStrictHostKeyChecking=no -p $SSH_PORT $SSH_USER@$SSH_HOST "\
	chmod +x /data/reed_vs_hamming-linux-x86; \
    chmod +x /data/bot.py; \
	cd /data/; \
    export EXPR=/data/reed_vs_hamming-linux-x86 \
        BOT_TOKEN=$BOT_TOKEN; \
	bash ./startup.sh;"
